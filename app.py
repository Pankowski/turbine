from parsers.classes.Parser import Parser
from parsers.classes.Parser import ParserModel
from parsers.classes.ParseGml import ParseGml
from parsers.classes.ParseXml import ParseXml
from parsers.classes.PolygonBuilder import PolygonBuilder
from parsers.classes.PointBuilder import PointBuilder
from parsers.classes.XmlBuilder import XmlBuilder

from models.Gmina import Gmina
from models.typBbox import TypBbox
from models.TurbinaWiatrowa import TurbinaWiatrowa
from models.typPolozenie import typPolozenie

# Rodzaje builderów
pb = PolygonBuilder()
pp = PointBuilder()
xb = XmlBuilder()

# Rodzaje parserów
pg = ParseGml()
px = ParseXml()

# Parser plików GML
parser = Parser(pg)

# Sparsowanie pliku gminy.gml i podanie kodu identyfikującego woj. łódzkie
gminy = parser.ParseFile(r'others\gminy.gml', '10')

# Transformacja parsera plików GML w parser plików XML
parser.ChangeParseType(px)

# Sparsowanie pliku PL.PZGIK.201.10__OT_BUWT_P.xml - turbiny wiatrowe
turbiny = parser.ParseFile(r'others\PL.PZGIK.201.10__OT_BUWT_P.xml')

# Zmapowanie wszystkich danych o gminach na obiekty
polyList = []
for gmina in gminy:

    name, ob, bbox = pb.BuildShape(gmina)
    left_down = typPolozenie(lon=bbox[0], lat=bbox[1])
    right_up = typPolozenie(lon=bbox[2], lat=bbox[3])

    polyList.append(Gmina(name, ob, TypBbox(left_down, right_up)))

# Zmapowanie wszystkich danych o turbinach na obiekty
pointList = []
for turbina in turbiny:

    height, point, lon, lat = pp.BuildShape(turbina)
    pointList.append(TurbinaWiatrowa(
        height, point, typPolozenie(lon=lon, lat=lat)))

# Sprawdzenie czy turbina znajduję się w danej gminie
for polygon in polyList:
    for point in pointList:
        polygon.CheckPosition(point)


# Zbudowanie xml'a
xb.buildXML(polyList, r'others\turbiny.xml')


from lxml import etree

def validate(xml_path: str, xsd_path: str):

    xmlschema_doc = etree.parse(xsd_path)
    xmlschema = etree.XMLSchema(xmlschema_doc)

    xml_doc = etree.parse(xml_path)
    result = xmlschema.validate(xml_doc)

    return result