
class Gmina():
    def __init__(self, nazwa='', shape=None, bbox=None):
        self.nazwa = nazwa
        self.shape = shape
        self.bbox = bbox
        self.turbiny = []
        self.liczba_turbin = len(self.turbiny)

    def ShowAll(self):
        return f'Gmina: {self.nazwa}, Położenie: {self.bbox}, Liczba turbin: {self.liczba_turbin}'

    def AddTurbine(self, turbina):
        self.turbiny.append(turbina)
        self.liczba_turbin = len(self.turbiny)

    def CheckPosition(self, turbinePoint):
        if self.shape.contains(turbinePoint.shape):
            self.AddTurbine(turbinePoint)
