import abc


class ParserAbstract():
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def ParseFile(self, *args):
        pass
