import abc


class ShapeBuilderAbstract():
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def BuildShape(self):
        pass
