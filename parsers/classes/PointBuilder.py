from parsers.abstract.ShapeBuilderAbstract import ShapeBuilderAbstract
from shapely.geometry import Point
from shapely.geometry.polygon import LinearRing


class PointBuilder(ShapeBuilderAbstract):
    def __init__(self):
        pass

    def BuildShape(self, data):

        return self.GetPoint(data)

    def GetPoint(self, data):

        point = Point(float(data['lon']), float(data['lat']))
        height = data['height']

        return height, point, data['lon'], data['lat']
