from parsers.abstract.ParserAbstract import ParserAbstract
import xml.etree.ElementTree as et


class ParseXml(ParserAbstract):

    def ParseFile(self, *args):
        data = []
        tree = et.ElementTree(file=args[0])
        featureMember = tree.findall(
            '{http://www.opengis.net/gml/3.2}featureMember')
        for i in featureMember:
            ob = {}
            turbine = i.find(
                '{urn:gugik:specyfikacje:gmlas:bazaDanychObiektowTopograficznych10k:1.0}OT_BUWT_P')

            if turbine.find('{urn:gugik:specyfikacje:gmlas:bazaDanychObiektowTopograficznych10k:1.0}x_kod').text == 'BUWT05':
                height = turbine.find(
                    '{urn:gugik:specyfikacje:gmlas:bazaDanychObiektowTopograficznych10k:1.0}wysokosc').text
                ob['height'] = height
                geometry = turbine.find(
                    '{urn:gugik:specyfikacje:gmlas:bazaDanychObiektowTopograficznych10k:1.0}geometria')
                point = geometry.find('{http://www.opengis.net/gml/3.2}Point')

                position = point.find(
                    '{http://www.opengis.net/gml/3.2}pos').text.split()

                ob['lon'] = position[0]
                ob['lat'] = position[1]

                data.append(ob)

        return data
