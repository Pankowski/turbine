from parsers.abstract.ParserAbstract import ParserAbstract
from lxml import etree


class ParseGml(ParserAbstract):

    def ParseFile(self, *args):
        communes = etree.parse(args[0]).getroot()

        data = communes.findall(
            './/gml:featureMember', namespaces=communes.nsmap)

        coordinatesCommunes = []

        for i in data:
            if str(i.find('.//prg:kodJednostki', namespaces=communes.nsmap).text)[0:2] == args[1]:

                ob = {}

                name = i.find('.//prg:nazwaJednostki',
                              namespaces=communes.nsmap).text
                ob['name'] = name

                exterior = i.find('.//gml:exterior',
                                  namespaces=communes.nsmap)
                exteriorValue = exterior.find('.//gml:posList',
                                              namespaces=communes.nsmap).text
                ob['exterior'] = [float(x) for x in exteriorValue.split()]

                interior = i.findall('.//gml:interior',
                                     namespaces=communes.nsmap)
                key = 0
                for j in interior:
                    interiorValue = j.find('.//gml:posList',
                                           namespaces=communes.nsmap).text
                    key = key + 1
                    strKey = f'interior{key}'
                    ob[strKey] = [float(x) for x in interiorValue.split()]

                coordinatesCommunes.append(ob)

        return coordinatesCommunes
