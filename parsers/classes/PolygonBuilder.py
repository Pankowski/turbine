from parsers.abstract.ShapeBuilderAbstract import ShapeBuilderAbstract
from shapely.geometry import Polygon
from shapely.geometry.polygon import LinearRing


class PolygonBuilder(ShapeBuilderAbstract):
    def __init__(self):
        pass

    def BuildShape(self, data):

        return self.GetPolygon(data)

    def GetLring(self, lring):

        lats = lring[::2]
        lons = lring[1::2]

        return LinearRing(zip(lons, lats))

    def GetPolygon(self, data):

        name = data['name']

        ext = self.GetLring(data['exterior'])

        inters = []
        interior = filter(lambda x: x[0:8] == 'interior', data.keys())
        for inter in list(interior):

            inter = self.GetLring(data[inter])
            inters.append(LinearRing(inter))

        poly = Polygon(LinearRing(ext), holes=inters)

        return name, poly, poly.bounds
