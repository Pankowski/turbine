import xml.etree.cElementTree as ET


class XmlBuilder():
    def __init__(self):
        self.pathOut = None

    def buildXML(self, data, pathOut):
        self.pathOut = pathOut
        gminy = ET.Element("gminy")

        for i in data:
            gmina = ET.SubElement(gminy, "gmina", liczba_turbin=str(
                i.liczba_turbin), nazwa=i.nazwa)
            for j in i.turbiny:
                if j.wysokosc is not None:
                    turbina = ET.SubElement(
                        gmina, "turbina", wysokosc=j.wysokosc)
                else:
                    turbina = ET.SubElement(gmina, "turbina")
                polozenie = ET.SubElement(
                    turbina, "polozenie", uklWsp="EPSG:2180")
                ET.SubElement(polozenie, "lon").text = str(j.polozenie.lon)
                ET.SubElement(polozenie, "lat").text = str(j.polozenie.lat)

            bbox = ET.SubElement(gmina, "bbox")
            lewy_dolny = ET.SubElement(bbox, "lewy_dolny", uklWsp="EPSG:2180")
            ET.SubElement(lewy_dolny, "lon").text = str(i.bbox.lewy_dolny.lon)
            ET.SubElement(lewy_dolny, "lat").text = str(i.bbox.lewy_dolny.lat)
            prawy_górny = ET.SubElement(
                bbox, "prawy_gorny", uklWsp="EPSG:2180")
            ET.SubElement(prawy_górny, "lon").text = str(
                i.bbox.prawy_gorny.lon)
            ET.SubElement(prawy_górny, "lat").text = str(
                i.bbox.prawy_gorny.lat)

        drzewo = ET.ElementTree(gminy)
        drzewo.write(self.pathOut, 'utf-8')
