class ParserModel():
    def __init__(self, parseType):
        self.parseType = parseType

    def ParseFile(self, *args):
        return self.parseType.ParseFile(*args)

    def ChangeParseType(self, parseType):
        self.parseType = parseType
