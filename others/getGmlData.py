from lxml import etree

path = r'others\gminy.gml'

gminy = etree.parse(path).getroot()

content = gminy.findall(
    './/prg:PRG_JednostkaPodzialuTerytorialnego', namespaces=gminy.nsmap)

counter = 0

for i in content:
    if str(i.find('.//prg:kodJednostki', namespaces=gminy.nsmap).text)[0:2] == '10':
        value = i.find('.//prg:nazwaJednostki', namespaces=gminy.nsmap).text
        print(f'<xs:enumeration value="{value}"/>')
        counter = counter + 1

print(f'Liczba gmin: {counter}')
